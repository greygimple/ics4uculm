# ICS4UCulm

<div align="center">
  <a href="https://gitlab.com/greygimple/ics4uculm">
    <img src="Screenshot.png?ref_type=heads" alt="Game Screenshot">
  </a>
</div>

## About The Project

Scripts from my grade 12 computer science course's culminating assignment. Can be repurposed for other projects

## Features

- [x] Six sample stages with hazards and moving objects

- [x] Feature rich and responsive character controller

- [x] Fluid camera movement that smoothly follows the player

## Prerequisites

- Unity 2022

## Download

```bash
git clone https://gitlab.com/greygimple/ics4uculm.git

cd ics4uculm
```

## Contact

Want to contribute and help add more features? Fork the project and open a pull request!

If you wish to contact me, you can reach out at greygimple@gmail.com
